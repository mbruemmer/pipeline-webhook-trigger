import json
import argparse


parser = argparse.ArgumentParser(description='Print parsed API payload for issue event webhook triggered pipeline')
parser.add_argument('payload', help='JSON payload of the pipeline trigger')

args = parser.parse_args()

payload = args.payload

try:
	with open(payload,"r") as payload_file:
		parsed_payload = json.load(payload_file)
		print("Attributes")
		print(parsed_payload["object_attributes"])
		print("Changes")
		print(parsed_payload["changes"])
		print("Full payload")
		print(parsed_payload)
except Exception as e:
	print("Can not read payload: %s" % e)